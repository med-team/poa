Source: poa
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>,
           Andreas Tille <tille@debian.org>,
           Étienne Mollier <emollier@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/med-team/poa
Vcs-Git: https://salsa.debian.org/med-team/poa.git
Homepage: http://poamsa.sourceforge.net/
Rules-Requires-Root: no

Package: poa
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Suggests: blast2
Enhances: t-coffee
Description: Partial Order Alignment for multiple sequence alignment
 POA is Partial Order Alignment, a fast program for multiple sequence
 alignment (MSA) in bioinformatics. Its advantages are speed,
 scalability, sensitivity, and the superior ability to handle branching
 / indels in the alignment. Partial order alignment is an approach to
 MSA, which can be combined with existing methods such as progressive
 alignment. POA optimally aligns a pair of MSAs and which therefore can
 be applied directly to progressive alignment methods such as CLUSTAL.
 For large alignments, Progressive POA is 10-30 times faster than
 CLUSTALW.
